/*-------------------------------------------------------------------
 * Práctica #3: Resolviendo problemas con Java Streams
 * Fecha: 14-Feb-2017
 * Autores:
 *          A01169735 Daniel Sada Caraveo
 *          A01372023 Kevin Islas Abud
 *-------------------------------------------------------------------*/

import java.util.Optional;
import java.util.stream.Stream;
import java.util.concurrent.ThreadLocalRandom;
import java.math.BigDecimal;

public class MonteCarloSerial {
    public static double sequentialMonteCarlo(int n) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            // Generar dos números aleatorios entre -1 y 1.
            double x = ThreadLocalRandom.current().nextDouble() * 2 - 1;
            double y = ThreadLocalRandom.current().nextDouble() * 2 - 1;

            // Aplicar teorema de Pitágoras.
            double h = x * x + y * y;

            // Verificar si el tiro cayó dentro del círculo.
            if (h <= 1) {
                sum++;
            }
        }
        return 4 * ((double) sum / n);
    }

    public static void main(String[] args){
        System.out.println(sequentialMonteCarlo(10_000_000));
    }
}