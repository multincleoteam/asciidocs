/*-------------------------------------------------------------------
 * Práctica #3: Resolviendo problemas con Java Streams
 * Fecha: 14-Feb-2017
 * Autores:
 *          A01169735 Daniel Sada Caraveo
 *          A01372023 Kevin Islas Abud
 *-------------------------------------------------------------------*/

import java.util.Optional;
import java.util.stream.Stream;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.concurrent.ThreadLocalRandom;

public class MonteCarloParalelo{
    public static double parallelMonteCarlo(int n) {

        int resultSuma = IntStream
            .range(1, n)
            .parallel()
            .map(d -> {
                return Math.pow(ThreadLocalRandom.current().nextDouble() * 2 - 1, 2) + Math.pow(ThreadLocalRandom.current().nextDouble() * 2 - 1, 2) <= 1 ? 1 : 0; 

            })
            .sum();
        return 4 * ((double) resultSuma / n);
    }

    public static void main(String[] args) {
        int n = 10_000_000;
        System.out.println(parallelMonteCarlo(n));
    }
}