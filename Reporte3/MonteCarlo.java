/*-------------------------------------------------------------------
 * Práctica #3: Resolviendo problemas con Java Streams
 * Fecha: 14-Feb-2017
 * Autores:
 *          A01169735 Daniel Sada Caraveo
 *          A01372023 Kevin Islas Abud
 *-------------------------------------------------------------------*/

import java.util.Optional;
import java.util.stream.Stream;
import javafx.util.Pair;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class MonteCarloParalelo{
    public static double parallelMonteCarlo(int n) {
        List<Double> elems = new ArrayList<Double>();
        new Random()
                .doubles(n*2,-1,1)
                .sequential()
                .forEach(elems::add);
        final Stream<Pair<Double, Double>> pairStream = IntStream
                .range(1, elems.size())
                .parallel()
                .filter(i -> i%2 ==0)
                .parallel()
                .mapToObj(i -> new Pair<Double, Double>(elems.get(i - 1), elems.get(i)));
        long resultSuma = pairStream
                .parallel()
                .mapToDouble(a -> (double)a.getKey()*(double)a.getKey()+(double)a.getValue()*(double)a.getValue())
                .filter(a -> {
                    if(a<1 && a > -1){
                        return true;
                    } else{
                        return false;
                    }
                }).count();
        return 4 * ((double) resultSuma / n);
    }

    public static void main(String[] args) {
        int n = 10_000_000;
        System.out.println(parallelMonteCarlo(n));
    }
}