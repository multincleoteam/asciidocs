/*-------------------------------------------------------------------
 * Práctica #3: Resolviendo problemas con Java Streams
 * Fecha: 14-Feb-2017
 * Autores:
 *          A01169735 Daniel Sada Caraveo
 *          A01372023 Kevin Islas Abud
 *-------------------------------------------------------------------*/

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.Random;

public class ConteoParalelo {

	public static Double[] order(double d, Stream<Double> s){

		AtomicInteger count = new AtomicInteger(0);
		s.forEach((n) -> {
			if(n < d) {
				count.getAndIncrement();
			}
		} );
		return new Double[]{d, count.doubleValue()};
	}

	public static void parallelCountSort(Integer array[]) {

		Double[] dArray = new Double[array.length];
		Integer[] sortedArr = new Integer[array.length];
		for(int i = 0; i < array.length; i++) {
			dArray[i] =  array[i].doubleValue() + (1.0 - (1.0 / (i+2)));
		}

		//Suplier de streams 
		Supplier<Stream<Double>> streamSupplier = () -> Stream.of(dArray);

		streamSupplier.get().parallel()
			.map(d -> order(d, streamSupplier.get()))
			.forEach((d) -> {
				sortedArr[d[1].intValue()] = d[0].intValue();
			});
		System.arraycopy(sortedArr, 0, array, 0, array.length);
	}

	public static void main(String[] args) {

		Random random = new Random();

		int size = 100_000;
		Integer[] array = new Integer[size];

		for(int i = 0; i < size; i++) {
			array[i] = random.nextInt(100);
		}

        parallelCountSort(array);
       
        int sorted = 1;
        for (int i = 0; i < size - 1; i++) { 
            if(array[i] > array[i + 1]) {
                sorted = 0;
            }
        }
        System.out.println(sorted);
    }
}