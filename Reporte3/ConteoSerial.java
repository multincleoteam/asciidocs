/*-------------------------------------------------------------------
 * Práctica #3: Resolviendo problemas con Java Streams
 * Fecha: 14-Feb-2017
 * Autores:
 *          A01169735 Daniel Sada Caraveo
 *          A01372023 Kevin Islas Abud
 *-------------------------------------------------------------------*/

import java.util.Optional;
import java.util.stream.Stream;
import java.util.Random;

public class ConteoSerial {
    public static void sequentialCountSort(int a[]) {
        final int n = a.length;
        final int[] temp = new int[n];
        for (int i = 0; i < n; i++) {
            int count = 0;
            for (int j = 0; j < n; j++) {
                if (a[j] < a[i]) {
                    count++;
                } else if (a[i] == a[j] && j < i) {
                    count++;
                }
            }
            temp[count] = a[i];
        }
        System.arraycopy(temp, 0, a, 0, n);
    }

    public static void main(String[] args) {

        Random random = new Random();

        int size = 100_000;
        int[] array = new int[size];

        for(int i = 0; i < size; i++) {
            array[i] = random.nextInt(100);
        }

        sequentialCountSort(array);

        int sorted = 1;
        for (int i = 0; i < size - 1; i++) { 
            if(array[i] > array[i + 1]) {
                sorted = 0;
            }
        }
        System.out.println(sorted);
    }
}