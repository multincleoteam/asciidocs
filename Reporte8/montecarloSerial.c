// ITESM CEM, April 4, 2017.
// C Source File
// Activity: Práctica #8: Resolviendo problemas con OpenMP
// Authors: 
//       A01169735 Daniel Sada Caraveo
//       A01372023 Kevin Islas Abud

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void monteCarloSerial(int n);

int main(int argc, char const *argv[])
{
    clock_t start = clock();
    monteCarloSerial(1000000000);
    clock_t end = clock();
    float seconds = (float)(end - start) / CLOCKS_PER_SEC;

    printf("Seconds: %f\n", seconds);

    return 0;
}

void monteCarloSerial(int n){
    int sum = 0;
    double x = 2;
    double y = 2;
    int seed = time(NULL);
    for (int i = 0; i < n; i++)
    {   

        x = ((double)rand_r(&seed)/(double)RAND_MAX )*2-1;
        y = ((double)rand_r(&seed)/(double)RAND_MAX )*2-1;       
        sum+= x*x + y*y <= 1;   
    }
    printf("Result: %f\n", 4 * ((double) sum / n));
}