// ITESM CEM, April 4, 2017.
// C Source File
// Activity: Práctica #8: Resolviendo problemas con OpenMP
// Authors: 
//       A01169735 Daniel Sada Caraveo
//       A01372023 Kevin Islas Abud

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define sz(x) sizeof(x)/sizeof(x[0])

void serialCount(int a[], size_t size);

int main(int argc, char const *argv[]) {
	int n = 50000;
	int arr[n];
	srand(time(NULL));
	for (int i = 0; i < n; i++) {
		arr[i] = rand() % 100;
	}
	clock_t start = clock();

	serialCount(arr, sz(arr));

	clock_t end = clock();
    float seconds = (float)(end - start) / CLOCKS_PER_SEC;

    int sorted = 1;
    for(int i = 0; i < (n-1); i++) {
    	if(arr[i] > arr[i+1]) {
    		sorted = 0;
    	}
    }
    printf("%s\n", (sorted ? "Sorted" : "Not sorted"));
    printf("Seconds: %f\n", seconds);
	return 0;
}	

void serialCount(int arr[], size_t size) {
	int n = size;
	int temp[n];

	for (int i = 0; i < n; i++) {

		int count = 0;
		float num = arr[i] + ((float) i / (n + 1));
		for (int j = 0; j < n; j++) {
			double numB = arr[j] + ((float) j / (n + 1));
			count += numB < num;
		}
		temp[count] = num;
	}

	for(int i = 0; i < n; i++) {
		arr[i] = (int) temp[i];
	}
}