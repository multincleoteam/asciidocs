// ITESM CEM, April 4, 2017.
// C Source File
// Activity: Práctica #8: Resolviendo problemas con OpenMP
// Authors: 
//       A01169735 Daniel Sada Caraveo
//       A01372023 Kevin Islas Abud

#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <time.h>

void monteCarloParallel(int n);

int main(int argc, char const *argv[])
{
    long long start = 0;
	long long end = 0;
	long long elapsed = 0;

	start = time(NULL);
    monteCarloParallel(1000000000);

	end = time(NULL);
	elapsed = end - start;

    printf("Seconds: %lld\n", elapsed);

    return 0;
}

void monteCarloParallel(int n){
    int sum = 0;

    #pragma omp parallel for num_threads(4) reduction(+:sum)
    for (int i = 0; i < n; i++)
    {   
    	int seed;
        double x = ((double)rand_r(&seed)/(double)RAND_MAX )*2-1;
        double y = ((double)rand_r(&seed)/(double)RAND_MAX )*2-1;

        sum+= x*x + y*y <= 1;   
    }
    printf("Resultado: %f\n", 4 * ((double) sum / n));
}