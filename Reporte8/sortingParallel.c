// ITESM CEM, April 4, 2017.
// C Source File
// Activity: Práctica #8: Resolviendo problemas con OpenMP
// Authors: 
//       A01169735 Daniel Sada Caraveo
//       A01372023 Kevin Islas Abud

#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#define sz(x) sizeof(x)/sizeof(x[0])

void parallelCount(int a[], size_t size);

int main(int argc, char const *argv[]) {
	int n = 50000;
	int arr[n];
	srand(time(NULL));
	for (int i = 0; i < n; i++) {
		arr[i] = rand() % 100;
	}

	long long start = 0;
	long long end = 0;
	long long elapsed = 0;
	start = time(NULL);

	parallelCount(arr, sz(arr));

	end = time(NULL);
	elapsed = end - start;

    int sorted = 1;
    for(int i = 0; i < (n-1); i++) {
    	if(arr[i] > arr[i+1]) {
    		sorted = 0;
    	}
    }
    printf("%s\n", (sorted ? "Sorted" : "Not sorted"));
    printf("Seconds: %lld\n", elapsed);
	return 0;
}

void parallelCount(int arr[], size_t size) {
	int n = size;
	int temp[n];

	#pragma omp parallel for num_threads(4)
	for (int i = 0; i < n; i++) {

		int count = 0;
		float num = arr[i] + ((float) i / (n + 1));
		for (int j = 0; j < n; j++) {
			double numB = arr[j] + ((float) j / (n + 1));
			count += numB < num;
		}
		temp[count] = num;
	}

	#pragma omp parallel for num_threads(4)
	for(int i = 0; i < n; i++) {
		arr[i] = (int) temp[i];
	}
}