/*-------------------------------------------------------------------
 * Práctica #1: Calculando Pi con threads en Java
 * Fecha: 24-Ene-2017
 * Autores:
 *          A01169735 Daniel Sada Caraveo
 *          A01372023 Kevin Islas Abud
 *-------------------------------------------------------------------*/

package mx.itesm;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SerialPi {

    public static void main(String[] args) {
        long timeStart = System.currentTimeMillis();
        long num_rects = 1_000_000;
        BigDecimal sum = BigDecimal.ZERO;

        BigDecimal mid, height, width, area;
        width = BigDecimal.ONE;
        width = width.divide(BigDecimal.valueOf(num_rects));

        for (long i = 0; i < num_rects; i++) {
            mid = BigDecimal.valueOf(i + 0.5).multiply(width);
            mid = mid.multiply(mid).add(BigDecimal.ONE);
            height = BigDecimal.valueOf(4.0);
            height = height.divide(mid,1000,RoundingMode.HALF_EVEN);
            sum = sum.add(height);
        }

        area = sum.multiply(width);
        long timeStop = System.currentTimeMillis();
        System.out.printf("Resultado = %.100f, Tiempo = %.4f", area, (timeStop-timeStart)/1000.0);
    }
}
