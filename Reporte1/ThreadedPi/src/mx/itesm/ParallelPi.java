/*-------------------------------------------------------------------
 * Práctica #1: Calculando Pi con threads en Java
 * Fecha: 24-Ene-2017
 * Autores:
 *          A01169735 Daniel Sada Caraveo
 *          A01372023 Kevin Islas Abud
 *-------------------------------------------------------------------*/

package mx.itesm;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ParallelPi implements Runnable{
    private int n; // Numero de procesadores que se manejan
    private static long num_rects = 1_000_000;
    public BigDecimal sum = BigDecimal.valueOf(0);

    public ParallelPi(int n) {
        this.n = n;
    }

    public void run() {
        BigDecimal mid, height, width;
        width = BigDecimal.ONE;
        width = width.divide(BigDecimal.valueOf(num_rects));

        for (long i = n; i < num_rects; i+=4) {
            mid = BigDecimal.valueOf(i + 0.5).multiply(width);
            mid = mid.multiply(mid).add(BigDecimal.ONE);
            height = BigDecimal.valueOf(4.0);
            height = height.divide(mid,1000,RoundingMode.HALF_EVEN);
            sum = sum.add(height);
        }

    }

    public static void main (String[] args) throws InterruptedException{
        long timeStart = System.currentTimeMillis();
        BigDecimal width;
        width = BigDecimal.ONE;
        width = width.divide(BigDecimal.valueOf(num_rects));
        ParallelPi p1 = new ParallelPi(0);
        ParallelPi p2 = new ParallelPi(1);
        ParallelPi p3 = new ParallelPi(2);
        ParallelPi p4 = new ParallelPi(3);
        Thread th1 = new Thread(p1);
        Thread th2 = new Thread(p2);
        Thread th3 = new Thread(p3);
        Thread th4 = new Thread(p4);
        th1.start();
        th2.start();
        th3.start();
        th4.start();
        th1.join();
        th2.join();
        th3.join();
        th4.join();
        BigDecimal resultado = BigDecimal.ZERO;
        resultado = resultado.add(p1.sum).add(p2.sum).add(p3.sum).add(p4.sum).multiply(width);
        long timeStop = System.currentTimeMillis();
        System.out.printf("Resultado = %.100f, Tiempo = %.4f", resultado, (timeStop-timeStart)/1000.0);
    }

}
