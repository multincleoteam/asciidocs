import os;
import time;
import sys;
from decimal import Decimal;

print("Introducir nombre/ruta de programa a ejecutar");
print("Ejemplo: testClass.java");

classPathWithName = raw_input().strip(); #Python < 3.0
#classPathWithName = input().strip() #Python 3.0

if(classPathWithName == ""):
	print("Error");
	sys.exit();

className = classPathWithName.split(".")[0];

os.popen("javac {0}".format(classPathWithName));

total = 0;
timeTotal = 0;
lastResult = 0;
for i in range(0, 5):
	timeStart = int(round(time.time() * 1000));
	response = os.popen("java {0}".format(className)).read();
	timeEnd = int(round(time.time() * 1000));
	total += Decimal(response);
	timeTotal += (timeEnd - timeStart);
	lastResult = Decimal(response);
	print("Corrida {0}: Tiempo: {1}".format(i+1, (timeEnd - timeStart)/1000.0 ))

os.system("rm {0}.class".format(className));
print("Resultado: {0}".format(lastResult));
print("Promedio tiempo: {0} milisegundos".format(timeTotal / 5000.0));