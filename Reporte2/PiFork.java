/*-------------------------------------------------------------------
 * Práctica #2: Calculando Pi con Fork/Join en Java
 * Fecha: 7-Feb-2017
 * Autores:
 *          A01169735 Daniel Sada Caraveo
 *          A01372023 Kevin Islas Abud
 *-------------------------------------------------------------------*/

import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ForkJoinPool;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class PiFork extends RecursiveTask<BigDecimal> {
	private int start;
	private int end;
	private int UMBRAL = 1_000;
	private BigDecimal width;
	
    static int num_rects = 1_000_000;
	public static void main(String[] args) {
    	
    	//long timeStart = System.currentTimeMillis();
        BigDecimal area = BigDecimal.ONE;
        PiFork task = new PiFork(0, num_rects);
		ForkJoinPool pool = new ForkJoinPool();
		
		BigDecimal resultado = pool.invoke(task);
		area = resultado.multiply(task.width);

		//long timeEnd = System.currentTimeMillis();
		//System.out.println(timeEnd - timeStart);
		System.out.println(area);

	}

	public PiFork(int start, int end) {
		this.start = start;
		this.end = end;
		
		this.width = BigDecimal.ONE;
        width = width.divide(BigDecimal.valueOf(num_rects));
	}
	
	@Override
	public BigDecimal compute() {
		if(end - start < UMBRAL) {
			BigDecimal resultado = BigDecimal.ZERO;
			BigDecimal mid, height;
		    for (long i = start; i < end; i++) {
                mid = BigDecimal.valueOf(i + 0.5).multiply(width);
                mid = mid.multiply(mid).add(BigDecimal.ONE);
                height = BigDecimal.valueOf(4.0);
                height = height.divide(mid,1000,RoundingMode.HALF_EVEN);
                resultado = resultado.add(height);
            }
			return resultado;
			
		} else {
			int mid = (start + end) >>> 1;
			PiFork t1, t2;
			t1 = new PiFork(start, mid);
			t2 = new PiFork(mid, end);

			t1.fork();
			return t2.compute().add(t1.join());
		}
	}
}