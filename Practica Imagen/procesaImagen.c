#include <stdio.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

int main(void) {
	int ancho, alto, canales;
	unsigned char *data = stbi_load("natalie.png", &ancho, &alto, &canales, 0);

	if(data) {
		printf("Ancho: %d\n", ancho);
		printf("Alto: %d\n", alto);
		printf("Canales %d\n", canales);
		stbi_image_free(data);	
	} else {
		fprintf(stderr, "No se pudo leer el archivo");
	}
	
	return 0;
}
