/*-------------------------------------------------------------------
 * Práctica #2: Calculando Pi con Fork/Join en Java
 * Fecha: 1-Feb-2017
 * Autores:
 *          A01169735 Daniel Sada Caraveo
 *          A01372023 Kevin Islas Abud
 *-------------------------------------------------------------------*/

import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ForkJoinPool;
import java.math.BigInteger;

public class ParallelFactorial extends RecursiveTask<BigInteger>{

	private static int num_rects = 300_000;
	private final int start;
	private final int end;
	private final int UMBRAL = 1_000;

	public static void main(String[] args) {

		ParallelFactorial p = new ParallelFactorial(2,num_rects+1);

		ForkJoinPool pool = new ForkJoinPool();

		BigInteger resultado = pool.invoke(p);

		System.out.println(resultado.bitCount());
	}

	public ParallelFactorial(int start, int end) {
		this.start = start;
		this.end = end;
	}

	@Override
	public BigInteger compute() {
		if(end - start < UMBRAL) {
			BigInteger resultado = BigInteger.ONE;

			for(int i = start; i < end; i++) {
				resultado = resultado.multiply(BigInteger.valueOf(i));
			}
			return resultado;
		} else {
			int mid = (start + end) >>> 1;
			ParallelFactorial t1, t2;
			t1 = new ParallelFactorial(start, mid);
			t2 = new ParallelFactorial(mid, end);

			t1.fork();
			return t2.compute().multiply(t1.join());
		}
	}

}