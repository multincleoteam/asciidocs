'use strict'

let n = 1;
busca: while(true) {
	n++;
	let i;
	for(i = 2; i<= Math.sqrt(n); i++) {
		if(n % i == 0) {
			continue busca;
		}
	}
	postMessage(i);
}

//python3 -m http.server