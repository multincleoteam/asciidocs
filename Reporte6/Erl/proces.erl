% ITESM CEM, March 22, 2017.
% Erlang Source File
% Activity: Práctica #6: Erlang concurrente
% Authors: 
% Daniel Sada Caraveo A01169735
% Kevin Islas Abud A01372023

-module(proces).
-export([double/1]).
-compile(proces).

factorial(N) ->
  P = spawn(fun aux/0),
  P ! {fact, N, self()},
  receive
    Result -> Result
  end.
  
aux() ->
  receive
    {fact, N, Remitente} -> Remitente ! fact_fun(N)
  end.
  
fact_fun(0) -> 1;
fact_fun(N) when N > 0 ->
  N * fact_fun(N - 1).
  
fibo_proc() ->
  spawn(fun () -> fibo_loop([1, 0]) end).
  
fibo_send(Pid, Mssg) ->
  case is_process_alive(Pid) of
    true ->
      Pid ! {Mssg, self()},
      receive
        X -> X
      end;
    false -> killed
  end.
  
fibo_loop(Nums) ->
  receive
    {recent, Remitente} ->
      Remitente ! hd(Nums),
      fibo_loop(Nums);
    {span, Remitente} ->
      Remitente ! length(Nums),
      fibo_loop(Nums);
    {_, Remitente} ->
      Remitente ! killed
    after 1000 ->
      [A, B | T] = Nums,
      fibo_loop([A + B, A, B | T])
  end.

double(M) ->
	P = spawn(fun double_pair/0),
	P ! {loop, M, 0, self()},
	receive
		{loop, M,N, Remitente} when M > N ->
			Remitente ! {loop, M,N+1, self()};
		{_, _, _, Remitente} ->
			Remitente ! killed 
	end.

double_pair()->
	receive
		{loop, M,N, Remitente} when M > N ->
			Remitente ! {loop, M,N+1, self()};
		{_, _, _, Remitente} ->
			Remitente ! killed 
	end.







