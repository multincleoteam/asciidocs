% ITESM CEM, March 22, 2017.
% Erlang Source File
% Activity: Práctica #6: Erlang concurrente
% Authors: 
% Daniel Sada Caraveo A01169735
% Kevin Islas Abud A01372023

-module(procs).
-compile(export_all).

factorial(N) ->
  P = spawn(fun aux/0),
  P ! {fact, N, self()},
  receive
    Result -> Result
  end.
  
aux() ->
  receive
    {fact, N, Remitente} -> Remitente ! fact_fun(N)
  end.
  
fact_fun(0) -> 1;
fact_fun(N) when N > 0 ->
  N * fact_fun(N - 1).
  
fibo_proc() ->
  spawn(fun () -> fibo_loop([1, 0]) end).
  
fibo_send(Pid, Mssg) ->
  case is_process_alive(Pid) of
    true ->
      Pid ! {Mssg, self()},
      receive
        X -> X
      end;
    false -> killed
  end.
  
fibo_loop(Nums) ->
  receive
    {recent, Remitente} ->
      Remitente ! hd(Nums),
      fibo_loop(Nums);
    {span, Remitente} ->
      Remitente ! length(Nums),
      fibo_loop(Nums);
    {_, Remitente} ->
      Remitente ! killed
    after 1000 ->
      [A, B | T] = Nums,
      fibo_loop([A + B, A, B | T])
  end.

double(0, Doub_PID) ->
	Doub_PID ! finished,
	erlang:display("finished");
double(N, Doub_PID) ->
	Doub_PID ! {doub, self(), N},
	receive
		{doubb, PID, N} -> 
			erlang:display([PID, "recieved mesg ",N])
	end,
	double(N-1, Doub_PID).

doubble() ->
	receive
		finished ->
			erlang:display("finished");
		{doub, Doub_PID, N} ->
			erlang:display([self(), "recieved mesg ",N]),
			Doub_PID ! {doubb, Doub_PID, N},
			doubble()
	end.

start_doubles(N) ->
	Doub_PID = spawn(procs, doubble, []),
	spawn(procs, double, [N, Doub_PID]).


%La idea principal de el ejercicio 4 se sacó de:
% http://trigonakis.com/blog/2011/05/26/introduction-to-erlang-message-passing/
% Hubo partes que no supimos hacer y las sacamos del blog anterior

start_ring(NumEntidades, Times) when NumEntidades > 1 ->
	erlang:display(["Current Process is", self()]),
	Entidad = [spawn(?MODULE, entity, [ID, self(),NumEntidades]) || ID <- lists:seq(1, NumEntidades)],
	erlang:display(Entidad),
	procs:setup(Entidad),
	repeat(Times, Entidad).

setup(N = [H | _]) ->
    setup_(N ++ [H]).

setup_([]) ->
    setup;
setup_([_]) ->
    setup;
setup_([EInicial, ESig | TodosE]) ->
	EInicial ! {self(), junta, ESig},
	setup_([ESig|TodosE]).    

repeat(0, _) ->
	done;
repeat(N, Entidad) ->
	hd(Entidad) ! {0, self()},
	repeat(N-1, Entidad).

entity(Num, _Creador, Max) ->
	erlang:display(["Created", self()]),
	receive
		{Crea, junta, Sig} ->
			%erlang:display([Num, Crea, Sig]),
			entity(Num, Crea, Sig, Max)
	end.

entity(Num, Creador, Sig, Max) ->
	receive
		{Val, PID} ->
			erlang:display([self(), " Recieved from ", PID]),
			if
				Val < Max ->
					%erlang:display(["Valor ->", Val]),
					Sig ! {Val + 1, self()},
					entity(Num, Creador, Sig, Max);
				true ->
				    case erlang:is_process_alive(Sig) of
					true ->
					    Sig ! {Val + 1, self()};
					_ ->
					    ok
				    end,
					%erlang:display("ok"),
					done
			end
	end.









