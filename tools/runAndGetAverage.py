import os;
import time;
import sys;

print("Introducir nombre/ruta de programa a ejecutar");
print("Ejemplo: testClass.java");

classPathWithName = raw_input().strip();

if(classPathWithName == ""):
	print("Error");
	sys.exit();

className = classPathWithName.split(".")[0];

os.popen("javac {0}".format(classPathWithName));

total = 0;
timeTotal = 0;
for i in range(0, 5):
	timeStart = int(round(time.time() * 1000));
	response = os.popen("java {0}".format(className)).read();
	timeEnd = int(round(time.time() * 1000));
	total += int(response);
	timeTotal += (timeEnd - timeStart);

os.system("rm {0}.class".format(className));
print("Promedio resultados: {0}".format(total / 5));
print("Promedio tiempo: {0} milisegundos".format(timeTotal / 5));