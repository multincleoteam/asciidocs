package mx.itesm;

/**
 * Created by danielsada on 2/8/17.
 */
@FunctionalInterface
interface MyFunInter{
    public abstract int f (int x);
}

public class Testerino {

    public static void sequentialCountSort(int a[]) {
        final int n = a.length;
        final int[] temp = new int[n];
        for (int i = 0; i < n; i++) {
            int count = 0;
            for (int j = 0; j < n; j++) {
                if (a[j] < a[i]) {
                    count++;
                } else if (a[i] == a[j] && j < i) {
                    count++;
                }
            }
            temp[count] = a[i];
        }
        System.arraycopy(temp, 0, a, 0, n);
    }

    public static void main(String[] args) {
        //int x[] = {1,2,5,6,342,23,4,7};
        //sequentialCountSort(x);
        MyFunInter y = new FunctionalInterface (int x) -> x + 2;
        int r = y.f(5);
        System.out.println(r);
    }

}