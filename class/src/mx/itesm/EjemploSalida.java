package mx.itesm;

public class EjemploSalida implements Runnable{
    private static int indice = 0;
    private static EjemploSalida[] arreglo = new EjemploSalida[2];
    @Override
    public void run() {
        for (int i=0; i<50;i++){
            System.out.println(Thread.currentThread().getName() + " " + i);
        }
    }

    public static void main(String[] args) throws InterruptedException{
        EjemploSalida es1 = new EjemploSalida();
        EjemploSalida es2 = new EjemploSalida();
        Thread t1 = new Thread(es1);
        Thread t2 = new Thread(es2);
        t2.start();
        t2.join();
        System.out.println("Wow termine");
        //t.join();
	// write your code here
    }
}
