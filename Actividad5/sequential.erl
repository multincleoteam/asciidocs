% ITESM CEM, March 7, 2017.
% Erlang Source File
% Activity: Sequential Erlang
% Authors: 
%          A01169735 Daniel Sada Caraveo
%          A01372023 Kevin Islas Abud

-module(sequential).
-compile(export_all).

% 1 but_Last
% -------------------------------------------- %
but_last([_]) ->
	[];
but_last([H | T]) when T/= [] ->
	[H] ++ but_last(T).

% 2 merge
% -------------------------------------------- %
merge([], []) ->
	[];
merge([L], [R]) when L =< R ->
	[L] ++ [R];
merge([L], [R]) when L > R ->
	[R] ++ [L];
merge([H | T], [X | Y]) when H =< X ->
	[H] ++ merge(T, [X | Y]);
merge([H | T], [X | Y]) when H > X ->
	[X] ++ merge([H | T], Y).

% 3 insert
% -------------------------------------------- %
insert(N, [H | [S | T]]) when H =< N, S =< N ->
	[H] ++ insert(N, [S | T]);
insert(N, [H | [S | T]]) when H =< N, S > N ->
	[H] ++ [N] ++ [S | T];
insert(N, [H | [S | T]]) when H > N, S > N ->
	[N] ++ [H] ++ [S | T];
insert(N, [H | T]) when H =< N, T == []->
	[H] ++ [N];
insert(N, []) ->
	[N];
insert(N, [H]) when N =< H ->
	[N] ++ [H];
insert(N, [H]) when N > H ->
	[H] ++ [N].

% 4 sort
% -------------------------------------------- %
sort([]) ->
	[];
sort([H]) ->
	[H];
sort([H | T]) ->
	insert(H, sort(T)).

% 5 binary
% -------------------------------------------- %
binary(N) when N == 0 ->
	[];
binary(N) ->
	binary(N div 2) ++ [N rem 2].

% 6 bcd
% -------------------------------------------- %

list_to_str([]) ->
	[];
list_to_str([H | T]) ->
	integer_to_list(H) ++ list_to_str(T).

append_zeroes(H) when length(H) == 4 ->
	H;
append_zeroes(H) ->
	append_zeroes([0] ++ H).

bcd(N) when is_number(N), N > 9 ->
	%list_to_str(append_zeroes(binary(N))).
	bcd(N div 10) ++ bcd(N rem 10);
bcd(N) when N =< 9 ->
	[list_to_str(append_zeroes(binary(N)))].

% 7 prime_factors
% -------------------------------------------- %

get_factor(N) ->
	get_factor(N, 2).

get_factor(N, F) when N rem F == 0 ->
	F;
get_factor(N, F) ->
	get_factor(N, F+1).

prime_factors(N) when N == 1 ->
 	[];
prime_factors(N) ->
	H = get_factor(N),
	%[H] ++ prime_factors(N div H).
	insert(H, prime_factors(N div H)).


% 8 compress
% -------------------------------------------- %
remove(_, []) ->
	[];
remove(E, [H | T]) when H == E ->
	remove(E, T);
remove(E, [H | T]) when H /= E ->
	[H | T].

compress([]) ->
	[];
compress([H | T]) ->
	[H] ++ compress(remove(H, T)).

% 9 encode
% -------------------------------------------- %
count(_, []) ->
	0;
count(E, [H | T]) when E == H ->
	1 + count(E, T);
count(_, [_ | _]) ->
	0.

encode([]) ->
	[];
encode([H | [S | T]]) when H /= S ->
	[H] ++ encode([S | T]);
encode([H | [S | T]]) when H == S ->
	[{count(H, [H | [S | T]]), H}] ++ encode(remove(H, [S | T]));
encode([H]) ->
	[H].

% 10 decode
% -------------------------------------------- %
decode([]) ->
	[];
decode([{A, B} | T]) when A == 2 ->
	[B] ++ [B] ++ decode(T);
decode([{A, B} | T]) ->
	[B] ++ decode([{A-1, B} | T]);
decode([H | T]) ->
	[H] ++ decode(T).
