% ITESM CEM, March 21, 2017.
% Erlang Source File
% Activity: Erlang Concurrente
% Authors: 
%          A01169735 Daniel Sada Caraveo
%          A01372023 Kevin Islas Abud

-module(procs).
-compile(export_all).

%Concurrent Factorial
factorial(N) ->
	P = spawn(fun aux/0),
	P ! {fact, N, self()},
	receive
		Result -> Result
	end.

aux() -> 
	receive
		{fact, N, Remitente} -> Remitente ! fact_fun(N)
	end.

fact_fun(0) -> 1;
fact_fun(N) when N > 0 ->
	N * fact_fun(N-1).

% Concurrent Fibonacci
fibo_proc() ->
	spawn(fun () -> fibo_loop([1, 0]) end).

fibo_send(Pid, Mssg) ->
	case is_process_alive(Pid) of
		true ->
			Pid ! {Mssg, self()},
			receive
				X -> X
			end;
		false -> killed
	end.

fibo_loop(Nums) ->
	receive
		{recent, Remitente} ->
			Remitente ! hd(Nums),
			fibo_loop(Nums);
		{span, Remitente} ->
			Remitente ! length(Nums),
			fibo_loop(Nums);
		{_, Remitente} ->
			Remitente ! killed
		after 1000 ->
			[A, B | T] = Nums,
			fibo_loop([A + B, A, B | T])
		end.

%Concurrent Double
double(N) ->
	A = spawn(fun double_aux/0),
	B = spawn(fun double_auxb/0),
	io:format("Created ~p~n", [A]),
	io:format("Created ~p~n", [B]),
	A ! {1, N, B},
	ok.

double_aux() ->
	receive
		{S, N, Remitente} when S > N ->
			io:format("~p finished~n", [self()]),
			Remitente ! {S, N, self()},
			exit("");
		{S, N, Remitente} when S =< N ->
			io:format("~p received message ~B/~B ~n", [self(), S, N]),
			Remitente ! {S, N, self()},
			double_aux()
		end.

double_auxb() ->
	receive
		{S, N, _} when S > N ->
		io:format("~p finished~n", [self()]),
		exit("");
		{S, N, Remitente} when S =< N ->
			io:format("~p received message ~B/~B ~n", [self(), S, N]),
			Remitente ! {S+1, N, self()},
			double_auxb()
		end.

%Concurrent Ring
ring(N, M) ->
	io:format("Current process is ~p~n", [self()]),
	ring(N, M, 1, []).

ring(N, M, S, L) when S < N ->
	A = spawn(fun ring_aux/0),
	io:format("Created ~p~n", [A]),
	ring(N, M, S+1, lists:append(L, [A]));
ring(N, M, S, [H | T]) when S == N -> 
	B = spawn(fun ring_aux/0),
	io:format("Created ~p~n", [B]),
	H ! {1, M, lists:append(T, [B]), H, lists:append([H], lists:append(T, [B]))},
	ok.


ring_aux() ->
	receive
		{S, N, [H | T], C, D}  when S < N, H /= [] ->
			io:format("~p received message ~B/~B ~n", [self(), S, N]),
			H ! {S, N, T, C, D},
			ring_aux();	
		{S, N, H, C, [D | E]} when S < N, H == [] ->
			io:format("~p received message ~B/~B ~n", [self(), S, N]),
			C ! {S+1, N, E, C, [D | E]},
			ring_aux();

		{S, N, [H | T], C, D}  when S == N, H /= [] ->
			io:format("~p received message ~B/~B ~n", [self(), S, N]),
			io:format("~p finished~n", [self()]),
			H ! {S, N, T, C, D},
			exit("");

		{S, N, H, _, _}  when S == N, H == [] ->
			io:format("~p received message ~B/~B ~n", [self(), S, N]),
			io:format("~p finished~n", [self()]),
			exit("")
		end.

%Concurrent Star
star(N, M) -> 
	io:format("Current process is ~p~n", [self()]),
	star(N, M, 0, []).

star(N, M, C, L) when C == 0 ->
	Center = spawn(fun star_center/0),
	io:format("Created Process ~p (Center)~n", [Center]),
	star(N, M, C + 1, lists:append(L, [Center]));

star(N, M, C, L) when C < M  ->
	Side = spawn(fun star_sides/0),
	io:format("Created Process ~p~n", [Side]),
	star(N, M, C + 1, lists:append(L, [Side]));

star(N, M, C, [H | T]) when C == M ->
	H ! {0, N, self(), T},
	ok.

star_center() ->
	receive
		{S, E, Remitente, T} when S == 0 ->
			io:format("~p received ~B/~B from ~p~n", [self(), S, E, Remitente]),

			star_center_aux(S+1, E, T, T)
		end.

counter(N) when N > 0 ->
	receive
		{_} -> 
			counter(N - 1)
	end;

counter(N) when N == 0 ->
	io:format("~p finished~n", [self()]),
	exit("").

star_center_aux(S, E, _, Sides) when S > E ->

	lists:foreach(fun(P) ->
		P ! {S, E, self()} end, Sides),

	counter(length(Sides));

star_center_aux(S, E, [H | T], Sides) when S =< E, H /= [] ->
	H ! {S, E, self()},

	receive
		{S, E, Remitente} ->
			io:format("~p received ~B/~B from ~p~n", [self(), S, E, Remitente]),
			star_center_aux(S, E, T, Sides)
		end;

star_center_aux(S, E, H, Sides) when H == [] ->
	star_center_aux(S + 1, E, Sides, Sides).

star_sides() ->
	receive
		{S, E, Remitente} when S =< E ->
			io:format("~p received ~B/~B from ~p~n", [self(), S, E, Remitente]),
			Remitente ! {S, E, self()},
			star_sides();
		{S, E, Remitente} when S > E ->
			io:format("~p finished~n", [self()]),
			Remitente ! {1},
			exit("")
		end.