defmodule Chat.Server do

  use GenServer

  # Crea un proceso con el servidor
  def start_link do
    # Se le asigna un nombre al proceso para no tener que referenciar al PID
    GenServer.start_link(__MODULE__, [], name: :chat_room)
  end

  def add_message(message) do
    GenServer.cast(:chat_room, {:add_message, message})
  end
  def get_messages do
    GenServer.call(:chat_room, :get_messages)
  end

  # Proceso Servidor
  def init(messages) do
    {:ok, messages}
  end
  def handle_cast({:add_message, new_message}, messages) do
    {:noreply, [new_message | messages]}
  end
  def handle_call(:get_messages, _from, messages) do
    {:reply, messages, messages}
  end
end

# Proceso supervisor
defmodule Chat.Supervisor do
  use Supervisor

  # Crea el proceso supervisor
  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end
  
  def init(_) do
    children = [
      worker(Chat.Server, [])
    ]

    supervise(children, strategy: :one_for_one)

    # strategy:
    # one_for_one si un proceso muere, el supervisor lo reinicia
    # one_for_all si un proceso muere, todos los procesos hijos son terminados y reiniciados
  end
end
