%%%
%%% Encoding de este archivo: UTF-8
%%%
%%% Plantilla para artículo de Programación multinúcleo
%%%
%%% FAVOR DE NO CAMBIAR FUENTES, MÁRGENES, ETC.
%%%

\documentclass[10pt,letterpaper,oneside]{article}
\usepackage[margin=2cm]{geometry}
\usepackage[utf8]{inputenc} % Caracteres con acentos.
\usepackage{endnotes}
\usepackage{epsfig}

\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt}

\begin{document}

\renewcommand\abstractname{Resumen}
\renewcommand\refname{Referencias}
\renewcommand{\notesname}{Notas}

\pagestyle{empty}

\title{Programación multinúcleo utilizando Elixir}
\author{
Kevin Islas Abud (A01372023)
\\
\normalsize Instituto Tecnológico y de Estudios Superiores de Monterrey \\
\normalsize Campus Estado de México \\
Atizapán de Zaragoza, Estado de México, México.
}
\date{24 de abril, 2017.}

\maketitle

\thispagestyle{empty}

\begin{abstract}
Investigación de los beneficios que ofrece Elixir para el desarrollo de aplicaciones multinúcleo.
En el siguiente documento se analizan las ventajas del lenguaje de programación Elixir desarrollado en el año 2011 por José Valim.

Se estudiaron los mecanismos que utiliza Elixir para la creación y manejo de procesos, manejo de memoria y comunicación de procesos para el desarrollo de aplicaciones multinúcleo.

Para ello se realizaron tres programas que utilizan métodos números para aproximar el valor de $\pi$, posteriormente se analizó el speedup obtenido y utilización de núcleos. 
\end{abstract}

\section{Introducción}
En la última década los desarrolladores de software se han encontrado con un nuevo reto, los procesadores dejaron de cumplir con la ley de Moore, que dice que el número de transistores se duplicaría cada 2 años, los desarrolladores de hardware, en busca de seguir creando computadoras más rápidas, comenzaron a introducir múltiples núcleos en computadoras de uso común, que antes era una práctica única para computadoras de alto rendimiento o servidores \cite{lunch}.

Dicha tendencia ha provocado cambios importantes en la industria del software, ya que los desarrolladores, en busca de obtener un rendimiento óptimo en sus aplicaciones han tenido que programar aplicaciones que aprovechen los múltiples núcleos del ordenador en el que se esté ejecutando su código. Esto implica nuevos retos, ya que los desarrolladores de software deben determinar las tareas durante la ejecución de código que sean paralelizables, además de definir métodos de sincronización, ya que al utilizar múltiples núcleos introduce problemas como race conditions, esto significa, que al no estar sincronizados los hilos de ejecución del programa el resultado varíe según el orden en el que los hilos hayan accedido a los datos.

Esto ha provocado una proliferación de lenguajes de programación que faciliten el desarrollo de aplicaciones multinúcleo, éstos en su mayoría son funcionales e inmutables, ya que la mutabilidad de los datos es una de las causas comunes de errores en aplicaciones multinúcleo.

Los lenguajes funcionales ofrecen un paradigma que facilita la programación multinúcleo, ya que, en su mayor parte, las funciones son puras, no tienen efectos secundarios, esto facilita la programación de aplicaciones que aprovechen múltiples núcleos, ya que las funciones no pueden modificar el estado de otro objeto del programa.

Los primeros lenguajes funcionales fueron \textit{IPL}~\cite{ipl}, Lisp y APL, hace más de 50 años, sin embargo, no fue hasta después del 2005, cuando la velocidad de los procesadores se estabilizó y se popularizaron los equipos con múltiples procesadores que los lenguajes funcionales se utilizaron en aplicaciones de gran escala, un ejemplo de ello es \textit{Twitter}~\cite{twitter}, que en el año 2011 migró su backend de Ruby on Rails a Scala, un lenguaje funcional y como resultado de ello mejoró la disponibilidad de sus servicios web y aceleró sus tiempos de busca en un 300\%.

En la última década han surgido lenguajes que han buscado adaptarse al cambio de arquitectura computacional, la popularización de los servicios en línea y las soluciones existentes de portabilidad, los dos más populares han sido Clojure y Elixir, Clojure siendo un dialecto de Lisp que se ejecuta sobre la JVM y Elixir siendo un lenguaje que se ejecuta sobre Erlang VM.

\section{Desarrollo}

Ya que Elixir se ejecuta sobre la máquina virtual de Erlang, busca aprovechar el modelo de Erlang de manejo de procesos, \textit{garbage collection}~\cite{gc} y concurrencia.

Un programa de Elixir se subdivide en procesos, cada proceso cuenta con un heap privado de memoria, aquellos objetos cuyo tamaño sea mayor a 64 bytes se almacenan en un heap compartido. De esta forma, el heap privado se puede liberar por el garbage collector de cada proceso sin detener la ejecución de otros procesos. Para los objetos almacenados en la memoria compartida los procesos únicamente almacenan una referencia en su heap privado, por lo que si un proceso necesita pasar un objeto mayor a 64 bytes a otro proceso únicamente debe pasar la referencia al objeto, y el heap compartido lleva un conteo de referencias a los procesos que lo están utilizando, por lo que en el momento en el que dicho conteo llega a cero la memoria del objeto se puede liberar.

Esto facilita la programación concurrente en Elixir, al proporcionar un mecanismo eficiente de transferencia de objetos, además de ello, cada proceso al tener su heap privado es thread-safe.

Cada proceso de Elixir es un proceso ligero dentro de la máquina virtual de Erlang, por lo que crear un proceso implica un costo computacional considerablemente menor a un proceso de sistema operativo, una aplicación de Elixir puede fácilmente ejecutar miles de procesos.

La independencia de los procesos hace que Elixir tenga una alta tolerancia a fallos, ya que si un proceso falla, no detiene la ejecución de ningún otro proceso, Elixir proporciona un módulo llamado Supervisor que define a un proceso que pueda monitorear la ejecución de otros procesos, por lo que en caso de que un proceso termine de forma inesperada se puede simplemente ejecutar otro proceso.

\subsection{Chat Supervisado}

El siguiente programa ilustra lo simple que es implementar el módulo supervisor con el fin desarrollar un programa que utiliza un proceso que funcione como servidor de un chat y otro proceso que funcione como supervisor, si el proceso del servidor termina, el proceso supervisor inmediatamente lo reinicia\cite{supervisor}.

\begin{verbatim}
defmodule Chat.Server do

  use GenServer

  # Crea un proceso con el servidor
  def start_link do
    # Se le asigna un nombre al proceso para no tener que referenciar al PID
    GenServer.start_link(__MODULE__, [], name: :chat_room)
  end

  def add_message(message) do
    GenServer.cast(:chat_room, {:add_message, message})
  end
  def get_messages do
    GenServer.call(:chat_room, :get_messages)
  end

  # Proceso Servidor
  def init(messages) do
    {:ok, messages}
  end
  def handle_cast({:add_message, new_message}, messages) do
    {:noreply, [new_message | messages]}
  end
  def handle_call(:get_messages, _from, messages) do
    {:reply, messages, messages}
  end
end

# Proceso supervisor
defmodule Chat.Supervisor do
  use Supervisor

  # Crea el proceso supervisor
  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end
  
  def init(_) do
    children = [
      worker(Chat.Server, [])
    ]

    supervise(children, strategy: :one_for_one)

    # strategy:
    # one_for_one si un proceso muere, el supervisor lo reinicia
    # one_for_all si un proceso muere, todos los procesos hijos son terminados y reiniciados
  end
end
\end{verbatim}

En la siguiente imagen se puede notar que a pesar de que el proceso muera, el supervisor inmediatamente lo reinicia.

\centerline{\epsfig{figure=images/Chat.png,width=5in}}

\subsection{Cálculo de $\pi$ secuencial}
Para calcular $\pi$ de forma secuencial para determinar el speedup con métodos de programación multinúcleo, para ello utilizaremos la siguiente fórmula:

\begin{equation}
\mbox{\Large\(
\pi \approx \frac{\sum_{i=1}^n \frac{4}{1 + ((x_{i} + 0.5) / n)^2 } }{n}
\)}
\end{equation}

El código del programa es el siguiente:

\begin{verbatim}
defmodule PiSecuential do

  defp get_pi_secuential(n) do
    width = 1.0 / n
    get_pi_secuential(0, n - 1, 0, width)
  end

  defp get_pi_secuential(i, n, acc, width) when i < n do
    mid = (i + 0.5) * width
    temp = acc + (4.0 / (1.0 + mid * mid))
    get_pi_secuential(i + 1, n, temp, width)
  end

  defp get_pi_secuential(i, n, acc, width) when i == n do
    area = width * acc
    IO.puts("Result #{area}")
  end

  def calculate_secuential(n) do 
    {time, _} = :timer.tc(fn -> get_pi_secuential(n) end)
    IO.puts("Finished in #{time / 1000000} seconds")
  end
end
\end{verbatim}

Utilizando 100,000,000 rectángulos obtenemos el siguiente resultado:

\begin{verbatim}
iex(1)> PiSecuential.calculate_secuential 100000000
Result 3.1415926335904265
Finished in 4.174001 seconds
:ok
\end{verbatim}

\subsection{Cálculo de $\pi$ paralelo utilizando Task Async/Await}

Elixir cuenta con un módulo llamado Task que tiene dos funciones que nos ayudarán a paralelizar de forma sencilla el algoritmo para calcular Pi, Task.Async nos permite mandar una función que se ejecutará de forma paralela, Task.Await nos permitirá esperar a que la tarea termine para calcular el resultado.

El código es el siguiente:

\begin{verbatim}
defmodule Pi do 

  def get_rect(n, width) do
    mid = (n + 0.5) * width
    4.0 / (1.0 + mid * mid)
  end

  def get_pi_parallel(n) do
    width = 1.0 / n
    sum = Enum.to_list 0 .. n - 1 
    |> Enum.map(&(Task.async(fn -> get_rect(&1, width) end)))
    |> Enum.map(&Task.await/1)
    area = width * Enum.sum(sum)
    IO.puts("Calculation of Pi with #{n} rectangles is: #{area}")
  end

  def calculate_parallel(n) do 
    {time, _} = :timer.tc(Pi, :get_pi_parallel, [n])
    IO.puts("Finished in #{time / 1000000} seconds")
  end
end
\end{verbatim}

Obtenemos el siguiente resultado:

\begin{verbatim}
iex(3)> Pi.calculate_parallel 1000000
Calculation of Pi with 1000000 rectangles is: 3.1415926535897643
Finished in 10.886764 seconds
\end{verbatim}

Podemos notar que a pesar de que utilizamos un número menor rectángulos, el programa tarda más en realizar el cálculo.

El motivo de que esto suceda es que a pesar de que los procesos en Elixir sean ligeros, tratar de ejecutar millones de procesos de forma simultánea introduce un overhead que hace que la ejecución del programa sea más lenta.

La solución a esto es limitar el número de procesos que vamos a ejecutar de forma simultánea, además de ello evitar usar la función Await y utilizar mensajes entre procesos para procesar los resultados de forma más eficiente.

\subsection{Cálculo de $\pi$ paralelo utilizando spawn}

Spawn crea un proceso que se ejecuta de forma asíncrona, en el siguiente proceso creamos 1,000 procesos que van a ser distribuidos por el sistema.

Cada proceso itera entre el número del proceso, aumentando en el número de procesos (0, 1000, 2000 ... N), de forma que el trabajo se divide uniformemente entre los procesos, cuando el proceso ha finalizado, manda un mensaje con el resultado al monitor, que se encarga de notificar al proceso central cuando todos los procesos hayan finalizado.

\begin{verbatim}
defmodule PiSpawn do

  def time(n) do
    {time, _} = :timer.tc(fn -> calculate(n) end)
    IO.puts("Finished in #{time/1000000} seconds")  
  end

  defp calculate(n) do
    
    thread_num = 1000
    monitor = spawn(fn -> monitor(0, thread_num, n, self()) end)

    spawn(fn -> spawner(monitor, thread_num, 0, n) end)

    ref = Process.monitor(monitor)
    receive do
        {:DOWN, ^ref, _, _, _} ->
        IO.puts "Monitor finished"
      end
  end

  defp spawner(monitor, threads, current_thread, n) when current_thread < threads do

    spawn(fn -> calculator_process(current_thread, n, monitor, 0) end)
    spawner(monitor, threads, current_thread + 1, n)
  end

  defp spawner(_, _, _, _) do 
  end

  defp monitor(sum, thread_num, n, parent) do
    receive do
      {x} when thread_num > 1 ->
        monitor(sum + x, thread_num - 1, n, parent)
      {x} when thread_num == 1 ->
        width = 1.0 / n
        r = (sum + x) * width
        IO.puts("Result #{r}")
    end
  end

  defp calculator_process(start, finish, monitor, sum)  when start <= finish do
    calculator_process(start+1000, finish, monitor, sum + get_rect(start, finish))
  end

  defp calculator_process(_, _, monitor, sum) do
    send(monitor, {sum})
  end

  defp get_rect(n, finish) do
    width = 1.0/finish
    mid = (n+0.5)*width
    temp = (4.0/(1.0+mid*mid))
    temp
  end
end
\end{verbatim}

Con 100,000,000 rectángulos el programa nos da el siguiente resultado:

\begin{verbatim}
iex(12)> PiSpawn.time 100000000
Result 3.141592673589793
Monitor finished
Finished in 3.590237 seconds
\end{verbatim}

\subsection{Resultados}

Los resultados obtenidos entre el programa secuencial y el programa que utiliza Spawn programas son los siguientes:

\begin{center}

    \begin{tabular}{|r|r|r|}
    \hline
    \hline
    $Rect\acute{a}ngulos$ & $Tiempo\ Secuencial$ & $Tiempo\ Spawn$\\
    \hline
    \hline
    1,000       &   1.75e-4   &   0.010494        \\
    10,000      &   8.76e-4   &   0.010924        \\
    100,000     &   0.007771  &   0.013235        \\
    1,000,000   &   0.007771  &   0.04505         \\
    10,000,000  &   0.428639  &   0.378168        \\
    100,000,000 &   4.13946   &   3.604342        \\
    1,000,000,000 & 43.525167 &   35.39028        \\

    \hline
    \end{tabular}
    
\end{center}  

Como es común en aplicaciones multinúcleo, el speedup no se puede apreciar hasta que la tarea es lo suficientemente grande para compensar el overhead de crear y sincronizar múltiples procesos, en este caso, el rendimiento de la aplicación paralela es mayor a la aplicación secuencial después de los 10,000,000 de rectángulos, alcanzando un speedup de 1.22.

La siguiente imagen muestra la actividad del procesador durante la ejecución del programa secuencial:

\centerline{\epsfig{figure=images/Secuential.png,height=2in, width=0.5in}}

La siguiente imagen muestra la actividad del procesador durante la ejecución del programa paralelo:

\centerline{\epsfig{figure=images/Parallel.png,height=2in, width=0.5in}}

\section{Conclusiones}

Elixir es un lenguaje que cuenta con módulos que permiten desarrollar aplicaciones multinúcleo de forma sencilla, su manejo de procesos y el hecho de que sea un lenguaje funcional puro facilita la coordinación entre los procesos, a pesar de no haber obtenido el speedup ideal, los cambios en el código que se realizaron para calcular $\pi$ utilizando los dos núcleos y cuatro hyperthreads del procesador fueron mínimos. 

Elixir es un lenguaje diseñado para facilitar el desarrollo de aplicaciones multinúcleo, el manejo de procesos de Elixir facilita desarrollar aplicaciones que ejecuten múltiples procesos simultáneamente, ya sea con el fin de mejorar el rendimiento del programa, como los ejemplos para calcular $\pi$ o brindar tolerancia a fallos, como fue demostrado en el servidor de chat.

A medida que la industria de software adopte los cambios que implica el fin de la ley de Moore, los lenguajes funcionales, como Elixir, serán más comunes para el desarrollo de aplicaciones multinúcleo, ya que por diseño brindan herramientas que facilitan la sincronización de procesos.

\section{Agradecimientos}

Agradezco al profesor Ariel Ortiz Ramírez por su dedicación y entusiasmo por fomentar el estudio de paradigmas de programación no convencionales, ya que considero que ello ha tenido una gran influencia en mi formación como ingeniero en sistemas computacionales.
También quiero agradecer al ingeniero Diego Galíndez Barreda por sus consejos durante la implementación de los programas desarrollados en el artículo.
Finalmente quiero agradecer a José Valim por desarrollar un lenguaje que combine la tolerancia a fallos y robustez de Erlang con patrones sintácticos de Ruby.

\section{Referencias}
 
\begin{thebibliography}{9}

\bibitem{lunch}
    GotW.ca: Herb Sutter
    \emph{The Free Lunch Is Over: A Fundamental Turn Toward Concurrency in Software.}
    http://www.gotw.ca/publications/concurrency-ddj.htm Accedido el 20 de abril del 2017.

\bibitem{ipl}
    Wikipedia.
    \emph{Information Processing Language.} \\
    https://en.wikipedia.org/wiki/Information\_Processing\_Language Accedido el 20 de abril del 2017.

\bibitem{twitter}
    Readwrite
    \emph{Twitter Engineer Talks About the Company’s Migration from Ruby to Scala and Java.}
    http://readwrite.com/2011/07/06/twitter-java-scala/ Accedido el 20 de abril del 2017.

\bibitem{gc}
    Hamidreza Soleimani's Blog
    \emph{Erlang Garbage Collection Details and Why It Matters.}
    https://hamidreza-s.github.io/erlang+garbage\%20collection\%20memory\%20layout\%20soft\%20realtime/2015/08/24/erlang-garbage-collection-details-and-why-it-matters.html/ Accedido el 20 de abril del 2017.

\bibitem{supervisor}
    AlphaSights
    \emph{Process registry in Elixir: A practical example.}
    https://m.alphasights.com/process-registry-in-elixir-a-practical-example-4500ee7c0dcc Accedido el 20 de abril del 2017.

\end{thebibliography}

\theendnotes

\end{document}
