defmodule PiSecuential do

	defp get_pi_secuential(n) do
		width = 1.0 / n
		get_pi_secuential(0, n - 1, 0, width)
	end

	defp get_pi_secuential(i, n, acc, width) when i < n do
		mid = (i + 0.5) * width
		temp = acc + (4.0 / (1.0 + mid * mid))
		get_pi_secuential(i + 1, n, temp, width)
	end

	defp get_pi_secuential(i, n, acc, width) when i == n do
		area = width * acc
		IO.puts("Result #{area}")
	end

	def calculate_secuential(n) do 
		{time, _} = :timer.tc(fn -> get_pi_secuential(n) end)
		IO.puts("Finished in #{time / 1000000} seconds")
	end
end

defmodule Pi do 

	defp get_rect(n, width) do
		mid = (n + 0.5) * width
		4.0 / (1.0 + mid * mid)
	end

	defp get_pi_parallel(n) do
		width = 1.0 / n
		sum = Enum.to_list 0 .. n - 1 
		|> Enum.map(&(Task.async(fn -> get_rect(&1, width) end)))
		|> Enum.map(&Task.await/1)
		area = width * Enum.sum(sum)
		IO.puts("Calculation of Pi with #{n} rectangles is: #{area}")
	end

	def calculate_parallel(n) do 
		{time, _} = :timer.tc(fn -> get_pi_parallel(n) end)
		IO.puts("Finished in #{time / 1000000} seconds")
	end
end

defmodule PiSpawn do

	def time(n) do
		{time, _} = :timer.tc(fn -> calculate(n) end)
		IO.puts("Finished in #{time/1000000} seconds")	
	end

	defp calculate(n) do
		
		thread_num = 1000
		monitor = spawn(fn -> monitor(0, thread_num, n, self()) end)

		spawn(fn -> spawner(monitor, thread_num, 0, n) end)

		ref = Process.monitor(monitor)
		receive do
  			{:DOWN, ^ref, _, _, _} ->
    		IO.puts "Monitor finished"
    	end
	end

	defp spawner(monitor, threads, current_thread, n) when current_thread < threads do

		spawn(fn -> calculator_process(current_thread, n, monitor, 0) end)
		spawner(monitor, threads, current_thread + 1, n)
	end

	defp spawner(_, _, _, _) do	
	end

	defp monitor(sum, thread_num, n, parent) do
		receive do
			{x} when thread_num > 1 ->
				monitor(sum + x, thread_num - 1, n, parent)
			{x} when thread_num == 1 ->
				width = 1.0 / n
				r = (sum + x) * width
				IO.puts("Result #{r}")
		end
	end

	defp calculator_process(start, finish, monitor, sum)  when start <= finish do
		calculator_process(start+1000, finish, monitor, sum + get_rect(start, finish))
	end

	defp calculator_process(_, _, monitor, sum) do
		send(monitor, {sum})
	end

	defp get_rect(n, finish) do
		width = 1.0/finish
		mid = (n+0.5)*width
		temp = (4.0/(1.0+mid*mid))
		temp
	end
end