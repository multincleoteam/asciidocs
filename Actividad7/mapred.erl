% ITESM CEM, March 28, 2017.
% Erlang Source File
% Activity: Práctica #7: MapReduce con Erlang
% Authors: 
%          A01169735 Daniel Sada Caraveo
%          A01372023 Kevin Islas Abud

-module(mapred).
-compile(export_all).

%primes

get_list(N, M, L) when N =< M ->
	get_list(N + 1, M, lists:append(L, [N]));
get_list(_, _, L) ->
	L.

is_prime(N) when N == 1 ->
	{false, N};
is_prime(N) when N == 2 ->
	{true, N};
is_prime(N) when N rem 2 == 0 ->
	{false, N};
is_prime(N) ->
	is_prime(3, N, math:sqrt(N)).

is_prime(S, N, R) when S =< R, N rem S /= 0 ->
	is_prime(S + 2, N, R);
is_prime(S, N, R) when S =< R, N rem S == 0 ->
	{false, N};
is_prime(_, N, _) ->
	{true, N}.

primes(N, M) ->
	R = plists:mapreduce(fun is_prime/1, get_list(N, M, [])),
	case lists:keyfind(true, 1, dict:to_list(R)) of
		{true, V} -> V;
		_ -> []
	end.

%apocalyptic

pow(_, M) when M == 0 -> 1;
pow(N, M) when M == 1 -> N;
pow(B, M) -> pow(B, B, M).

pow(A, _, M) when M == 1 -> A;
pow(A, B, M) -> pow(A * B, B, M - 1). 
	
is_apocalyptic(N) when (N) == 0 -> false;
is_apocalyptic(N) when (N rem 1000) == 666 -> true;
is_apocalyptic(N) -> is_apocalyptic(N div 10).

aux_apocalyptic(N) ->
	case is_apocalyptic(pow(2, N)) of
		true -> {true, N};
		_ -> {false, N}
	end.

apocalyptic(N, M) ->
	R = plists:mapreduce(fun aux_apocalyptic/1, get_list(N, M, [])),
	case lists:keyfind(true, 1, dict:to_list(R)) of
		{true, V} -> V;
		_ -> []
	end.

% sexy

aux_sexy(N) ->
	case {is_prime(N), is_prime(N + 6), is_prime(N + 12)} of
		{{true, A}, {true, B}, {true, C}} -> {true, {A, B, C}};
		_ -> {false, {}}
	end.

sexy(N, M) ->
	R = plists:mapreduce(fun aux_sexy/1, get_list(N, M, [])),
	case lists:keyfind(true, 1, dict:to_list(R)) of
		{true, V} -> V;
		_ -> []
	end.

%phi13

sum_digits(N, R) when N == 0 -> R;
sum_digits(N, R) -> sum_digits(N div 10, (N rem 10) + R).

aux_phi13(N) ->
	R = sum_digits(pow(2, N), 0) rem 13,
	case R of
		0 -> {true, N};
		_ -> {false, N}
	end.

phi13(N, M) ->
	R = plists:mapreduce(fun aux_phi13/1, get_list(N, M, [])),
	case lists:keyfind(true, 1, dict:to_list(R)) of
		{true, V} -> V;
		_ -> []
	end.

