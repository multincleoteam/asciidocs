#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <limits.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define NANOSECS_PER_SEC 1000000000

long get_time() {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return t.tv_sec * NANOSECS_PER_SEC + t.tv_nsec;
}

struct rgb {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

int main(void) {

	int alpha = 155;
	
	//Imagen uno
    int ancho, altura, canales;
    struct rgb *data = (struct rgb *) stbi_load(
        "scarlett.png", &ancho, &altura, &canales, 0);

    //Imagen dos
    int anchoFlores, alturaFlores, canalesFlores;
    struct rgb *flowers = (struct rgb *) stbi_load(
    	"flores.png", &anchoFlores, &alturaFlores, &canalesFlores, 0);

    if (data != NULL && flowers!= NULL) {
        printf("Ancho = %d\n", ancho);
        printf("Alto = %d\n", altura);
        printf("Canales = %d\n", canales);

       	int num_pixels = ancho * altura;

        long inicio = get_time();

       	for(int i = 0; i < num_pixels - 1; i++) {
       		data[i].red =  (data[i].red * alpha) / 256 + (flowers[i].red * (255 - alpha)) / 256;
       		data[i].green =  (data[i].green * alpha) / 256 + (flowers[i].green * (255 - alpha)) / 256;
       		data[i].blue =  (data[i].blue * alpha) / 256 + (flowers[i].blue * (255 - alpha)) / 256;
       	}

        long final = get_time();
        printf("T secuencial = %.4f\n",
               ((double) final - inicio) / NANOSECS_PER_SEC);

        int r = stbi_write_png(
            "mix.png",
            ancho,
            altura,
            3,
            data,
            3 * ancho);
        if (r) {
            printf("Sí se pudo!\n");
        } else {
            printf(":'(");
        }

        stbi_image_free(data);
        stbi_image_free(flowers);
    }
    
    return 0;
}