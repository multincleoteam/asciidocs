section .text

;extern void procesa_imagen_simd(
;    uint8_t * data,						;;; rdi
;    uint8_t * data,						;;; rsi
;    uint8_t * constante_aclaramiento,		;;; rdx
;    uint8_t * constante_aclaramiento2,		;;; rcx
;    int num_pixels);						;;; r8


global procesa_imagen_simd

procesa_imagen_simd:
   	movdqu xmm3, [rdx]							; Mueve constante aclaramiento1 a xmm3
    movdqu xmm4, [rcx]							; Mueve constante aclaramiento2 a xmm3
    shr r8, 3   								; r8 <- r8 / 16 (shift 3 bits) numero pixeles
.ciclo:

	movdqu xmm1, [rdi]							; Imagen a xmm1
	pmovzxbw xmm1, xmm1							; convertir a palabras para multiplicar
	pmullw xmm1, xmm3							; multiplicar por xmm3 (alpha)
	psrlw xmm1, 8								; dividir por 256

	movq xmm2, [rsi]							; Imagen 2 a xmm2
	pmovzxbw xmm2, xmm2							; convertir a palabras
	pmullw xmm2, xmm4							;multiplicar por alpha 2 (255 - alpha)
	psrlw xmm2, 8								; dividir por 256

	paddw xmm1, xmm2							; sumar ambos paquetes de bits
	packuswb xmm1, xmm1							; empacar a bytes de nuevo

    movq [rdi], xmm1 							; mueve la suma a memoria
    add rdi, 8									; incrementa en 8 imagen 1
    add rsi, 8									; incrementa en 8 imagen 2
    dec r8										; resta contador
	jnz .ciclo									; repite hasta que num_pixels == 0
	ret