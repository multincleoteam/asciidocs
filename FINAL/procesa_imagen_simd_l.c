#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <limits.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define NANOSECS_PER_SEC 1000000000

long get_time() {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return t.tv_sec * NANOSECS_PER_SEC + t.tv_nsec;
}

extern void procesa_imagen_simd(
    uint8_t * data,
    uint8_t * data2,
    uint16_t * constante_aclaramiento,
    uint16_t * constante_aclaramiento2,
    uint32_t num_pixels);


int main(void) {

	int alpha = 155;
	
	//Imagen uno
    int ancho, altura, canales;
    uint8_t * data = (uint8_t *) stbi_load(
        "scarlett.png", &ancho, &altura, &canales, 0);

    //Imagen dos
    int anchoFlores, alturaFlores, canalesFlores;
   	uint8_t * flowers = (uint8_t *) stbi_load(
    	"flores.png", &anchoFlores, &alturaFlores, &canalesFlores, 0);

   	if (data != NULL && flowers!= NULL) {
        printf("Ancho = %d\n", ancho);
        printf("Alto = %d\n", altura);
        printf("Canales = %d\n", canales);

       	uint32_t num_pixels = ancho * altura;

        uint16_t cte[16];
        for (int i = 0; i < 16; i++) {
            cte[i] = alpha;
        }

        uint16_t cte2[16];
        for (int i = 0; i < 16; i++) {
            cte2[i] = 255-alpha;
        }


       	long inicio = get_time();
        procesa_imagen_simd((uint8_t *)data, flowers, cte, cte2, num_pixels * 3);
        long final = get_time();

        printf("T SIMD = %.4f\n",
               ((double) final - inicio) / NANOSECS_PER_SEC);

        int r2 = stbi_write_png(
            "mix2.png",
            ancho,
            altura,
            3,
            data,
            ancho * 3);
        if (r2) {
            printf("Sí se pudo!\n");
        } else {
            printf(":'(");
        }

        stbi_image_free(data);
        stbi_image_free(flowers);
    }


    return 0;
}