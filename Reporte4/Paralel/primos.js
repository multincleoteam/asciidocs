"use strict";
let sumatoria = 0;

let num = 100000000;
let cores = 4;
let finalizedWorkers = 0;
let start = new Date().getTime();
$(()=> {

	let arrWorkers = [];
	for(var i = 0; i < cores; i++) {
		arrWorkers[i] = new Worker("coprimos_worker.js");
	}
	let slice = num/arrWorkers.length;

	for (var i = arrWorkers.length - 1; i >= 0; i--) {
		arrWorkers[i].onmessage = messageHandler;
		arrWorkers[i].postMessage([slice*i, slice*(i+1)]);
	};
})

function messageHandler(event) {
	sumatoria += event.data;
	finalizedWorkers +=1;

	if(finalizedWorkers == cores) {
			let end = new Date().getTime();
			let time = end - start;
			$("#resultado").text(sumatoria + ", Tiempo : " + time/1000 + " segundos");
	} else {
		$("#resultado").text(sumatoria);
	}
}