'use strict'

addEventListener("message", function(event) {
	let i;
	postMessage(compute(event.data[0], event.data[1]));
}, false);

function gcd(a, b) {
    if ( a == 0 ) return b;
  
    return gcd(b % a, a);
}

function compute(start, end) {
	let sumatoria = 0;
	for(var i = start; i < end; i++) {
		if(gcd(i, 666) == 1) {
			sumatoria += i;
		}
	}
	return sumatoria;
}