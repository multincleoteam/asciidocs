'use strict'

let i;
let sumatoria = 1;
for(i = 2; i<= 100000000; i++) {
	if(gcd(i, 666) == 1) {
		sumatoria += i;
	}
}
postMessage(sumatoria);

function gcd(a, b) {
    if ( a == 0 ) return b;
  
    return gcd(b % a, a);
}