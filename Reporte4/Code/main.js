"use strict";

let sumatoria = 0;
$(()=> {
	let start = new Date().getTime();

	let w = new Worker("coprimos_worker.js")
	w.onmessage = (event) => {

		sumatoria += event.data
		let end = new Date().getTime();
		let time = end - start;
		$("#resultado").text(sumatoria + ", Tiempo : " + time/1000 + " segundos");
	}
})